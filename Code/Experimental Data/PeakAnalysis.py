import scipy.signal
import pandas
from pandas import DataFrame
import heapq
from DataProvider import DataProvider

def PeakAnalysis(speciesForPeakAdjustment):
    timeSeriesLists = speciesForPeakAdjustment

    timeSeriesA1 = timeSeriesLists[0] #Cln A
    timeSeriesA2 = timeSeriesLists[1] #Clb A
    timeSeriesB1 = timeSeriesLists[2] #Cln B
    timeSeriesB2 = timeSeriesLists[3] #Clb B

    peaksA1, propsA1 = scipy.signal.find_peaks(timeSeriesA1, distance = 2, prominence=0)
    peaksA2, propsA2 = scipy.signal.find_peaks(timeSeriesA2, distance = 2, prominence=0)
    peaksB1, propsB1 = scipy.signal.find_peaks(timeSeriesB1, distance = 2, prominence=0)
    peaksB2, propsB2 = scipy.signal.find_peaks(timeSeriesB2, distance = 2, prominence=0)

    peaksA1 = DataFrame(peaksA1, columns=['Peak'])
    peaksA2 = DataFrame(peaksA2, columns=['Peak'])
    peaksB1 = DataFrame(peaksB1, columns=['Peak'])
    peaksB2 = DataFrame(peaksB2, columns=['Peak'])

    promA1 = DataFrame(propsA1['prominences'], columns=['Prominence'])
    promA2 = DataFrame(propsA2['prominences'], columns=['Prominence'])
    promB1 = DataFrame(propsB1['prominences'], columns=['Prominence'])
    promB2 = DataFrame(propsB2['prominences'], columns=['Prominence'])

    peakPromA1 = pandas.concat([peaksA1, promA1], axis=1)
    peakPromA2 = pandas.concat([peaksA2, promA2], axis=1)
    peakPromB1 = pandas.concat([peaksB1, promB1], axis=1)
    peakPromB2 = pandas.concat([peaksB2, promB2], axis=1)

    peakPromA1 = peakPromA1.nlargest(5, 'Prominence') #get the 5 largest peaks
    peakPromA2 = peakPromA2.nlargest(5, 'Prominence') #get the 5 largest peaks
    peakPromB1 = peakPromB1.nlargest(5, 'Prominence') #get the 5 largest peaks
    peakPromB2 = peakPromB2.nlargest(5, 'Prominence') #get the 5 largest peaks


    matchesA = []
    for i in range(len(peakPromA1.index)):
        for j in range(len(peakPromA2.index)):
            if peakPromA1.iloc[i, 0] == peakPromA2.iloc[j, 0]:
                matchesA.append(peakPromA1.iloc[i,0])
            elif peakPromA1.iloc[i, 0] == peakPromA2.iloc[j, 0] + 1:
                matchesA.append(peakPromA1.iloc[i,0])
            elif peakPromA1.iloc[i, 0] == peakPromA2.iloc[j, 0] - 1:
                matchesA.append(peakPromA1.iloc[i,0])

    matchesB = []
    for i in range(len(peakPromB1.index)):
        for j in range(len(peakPromB2.index)):
            if peakPromB1.iloc[i, 0] == peakPromB2.iloc[j, 0]:
                matchesB.append(peakPromB1.iloc[i,0])
            elif peakPromB1.iloc[i, 0] == peakPromB2.iloc[j, 0] + 1:
                matchesB.append(peakPromB1.iloc[i,0])
            elif peakPromB1.iloc[i, 0] == peakPromB2.iloc[j, 0] - 1:
                matchesB.append(peakPromB1.iloc[i,0])


    #if a peak appears twice, something fishy is going on in the neighbourhood. Discard it.

    while True:
        if matchesA.count(min(matchesA)) > 1:
            matchesA = list(filter(lambda a: a != min(matchesA), matchesA))
        break;
    while True:
        if matchesB.count(min(matchesB)) > 1:
            matchesB = list(filter(lambda a: a != min(matchesB), matchesB))
        break;


    firstPeakA = min(matchesA)
    firstPeakB = min(matchesB)

    return (firstPeakA, firstPeakB)